public class Board
{
	//fields for Board
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	//constructor for Board
	public Board()
	{
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	//toString method to return String representation of tiles
	public String toString()
	{
		String empty = "";
		
		for(int i = 0; i < this.tiles.length; i++)
		{
			if(this.tiles[i] == false)
			{
				empty = empty + this.tiles[i];
			}
			else
			{
				empty = empty + "x";
			}
		}
		return empty;
	}
	//the playATurn instance method
	public boolean playATurn()
	{
		this.die1.roll();
		this.die2.roll();
		
		System.out.println(this.die1);
		System.out.println(this.die2);
		
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		
		for(int i = 0; i < this.tiles.length; i++)
		{
			if(tiles[sumOfDice - 1] = false)
			{
				tiles[i] = true;
				System.out.println("Closing tile equal to sum: " + tiles[i]);
				return false;
			}
			else if (tiles[die1.getFaceValue()] = false)
			{
				tiles[i] = true;
				System.out.println("Closing tile with the same value as die one: " + tiles[i]);
				return false;
			}
			else if (tiles[die2.getFaceValue()] = false)
			{
				tiles[i] = true;
				System.out.println("Closing tile with the same value as die two: " + tiles[i]);
				return false;
			}
			else
			{
				System.out.println("All the tiles for these values are already shut");
			}
		}
		return true;
	}
}