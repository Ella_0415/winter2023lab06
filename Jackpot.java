public class Jackpot
{
	public static void main(String[] args)
	{
		System.out.println("Welcome to the Jackpot game!");
		
		Board newBoard = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver = false)
		{
			System.out.println(newBoard);
			boolean boardCall = newBoard.playATurn();
			
			if(boardCall = true)
			{
				gameOver = true;
			}
			else
			{
				numOfTilesClosed++;
			}
		}
		if(numOfTilesClosed >= 7)
		{
			System.out.println("You have successfully reached the jackpot and won the game! Congratulations");
		}
		else
		{
			System.out.println("What a pity...you have lost the game");
		}
	}
}