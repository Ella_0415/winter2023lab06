import java.util.Random;
public class Die
{
	//fields for Die
	private int faceValue;
	private Random randomNum;
	
	//constructor for die
	public Die()
	{
		this.faceValue = 1;
		this.randomNum = new Random();
	}
	//get method for faceValue field
	public int getFaceValue()
	{
		return this.faceValue;
	}
	//roll method using faceValue and randomNum fields
	public int roll()
	{
		this.faceValue = randomNum.nextInt(1,7);
		return this.faceValue;
	}
	//toString method
	public String toString()
	{
		return "Die face value:" + this.faceValue;
	}
}